//
//  SecondCustomTableViewCell.h
//  TableViewExample
//
//  Created by LNT Chirag on 6/26/15.
//  Copyright (c) 2015 LNT Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondCustomTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelText;
@property (strong, nonatomic) IBOutlet UILabel *subTitleText;

@end
