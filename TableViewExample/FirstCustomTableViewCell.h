//
//  FirstCustomTableViewCell.h
//  TableViewExample
//
//  Created by LNT Chirag on 6/26/15.
//  Copyright (c) 2015 LNT Chirag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstCustomTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelText;
@property (strong, nonatomic) IBOutlet UILabel *detailLabelText;

@property (strong, nonatomic) IBOutlet UIImageView *imageDetail;
@end
